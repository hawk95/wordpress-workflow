var path       = require('path') ,
	projectName = path.basename( __dirname ),
	buildDir    = './build/',
	buildGlobs  =  [
							'**/*.*',

							// Mefia files
							'screenshot.{png,jpg}',

							// Ignore
							'!' + projectName + '.zip',
							'!gulpfile.js',
							'!package.json',
							'!.*',
							'!node_modules/**',
							'!scss/**'
						];

var gulp			= require('gulp'),
	sass			= require('gulp-sass'),
	autoprefix	= require('gulp-autoprefixer'),
	uglify		= require('gulp-uglify'),

	es 			= require('event-stream'),
	browserSync = require('browser-sync'),
	reload		= browserSync.reload,
	filter		= require('gulp-filter'),
	notify		= require('gulp-notify'),
	sequence		= require('gulp-run-sequence'),
	zip			= require('gulp-zip'),
	del			= require('del');


/* Keep browser sync with changes
******************************** */
gulp.task('browser-sync', function(){
	var files = [
						'**/*.php',
						'**/*.css',
					];

	browserSync.init( files, {
		proxy: 'localhost/wp/' + projectName,
		notify: false,
		injectChanges: true
	});
});

gulp.task('styles', function(){
	const front 	= 'scss/theme/styles.scss',
			builder 	= 'scss/builder/builder.scss',
			admin		= 'scss/admin/admin.scss';

	return es.merge(
		gulp.src(front)
		.pipe( sass({outputStyle:'compressed'}) )
		.pipe( autoprefix({ browsers: [ 'last 2 version', 'ie >= 9' ] }) )
		.pipe( gulp.dest('./resources/front/css/') ),

		gulp.src(admin)
		.pipe( sass({outputStyle:'compressed'}) )
		.pipe( autoprefix({ browsers: [ 'last 2 version', 'ie >= 9' ] }) )
		.pipe( gulp.dest('./resources/admin/css/') ),

		gulp.src(builder)
		.pipe( sass({outputStyle:'compressed'}) )
		.pipe( autoprefix({ browsers: [ 'last 2 version', 'ie >= 9' ] }) )
		.pipe( gulp.dest('./resources/admin/css/') )
	);
});

/* Deploy task
******************************** */
gulp.task('cleanup', function(){
	return del( buildDir + '/**');
});

gulp.task('copy', function(){
	const js 	= filter('**/*.js', {restore: true}),
			frontend	= filter('**/frontend.js', {restore: true}),
			admin		= filter('**/admin.js', {restore: true});

	return gulp.src(buildGlobs)
		.pipe(js)
		.pipe(uglify())
		.pipe( gulp.dest( buildDir + projectName ))
		.pipe(js.restore)

		.pipe( gulp.dest( buildDir + projectName ));
});

gulp.task('archive', function(){
	return gulp.src( path.dirname(buildDir + projectName) + '/' + projectName + '/**/*' )
		.pipe(zip( projectName + '.zip' ))
		.pipe(gulp.dest('./'))
		.pipe(notify({message:'Theme archived', onLast: true}));
});

gulp.task('build', function(){
	return sequence( 'styles', 'copy', 'archive', 'cleanup');
});


gulp.task('default', ['styles', 'browser-sync'], function(){
	gulp.watch( ['**/*.scss', '**/*.js'], ['styles', browserSync.reload] );
});